import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { NgZorroAntdMobileModule } from 'ng-zorro-antd-mobile';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { ArticleViewPage } from './ui/social/articles/article-view/article-view.page';
import { SafeContentPipe } from './utilities/pipes/safe-content.pipe';

@NgModule({
  declarations: [AppComponent, SafeContentPipe],
  entryComponents: [],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot({ name:'__mydb', driverOrder:["localstorage"] })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ArticleViewPage,
    NgZorroAntdMobileModule,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  exports:[],
  bootstrap: [AppComponent]
})
export class AppModule {}
