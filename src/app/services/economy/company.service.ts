import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  userToken: string;

  constructor(private httpClient: HttpClient, private storage: Storage) {
    this.storage.get("userToken").then(x => { this.userToken = x; });
   }

   getEmployment() : Observable<any> {
    return this.httpClient.get(environment.ApiUrl + "/api/company/GetEmployment?token=" + this.userToken);
  }
}
