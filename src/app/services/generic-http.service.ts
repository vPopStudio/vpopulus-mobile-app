import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class GenericHttpService {

  constructor(
    private http: HttpClient,
    private storage: Storage,
  ) { }

  async get(path) {
    var token = await this.storage.get("userToken");
    return this.http.get<any>(`${environment.ApiUrl}${path}`, { headers: { 'token': `${token}` } } );
  }

  async post(path, payload) {
    var token = await this.storage.get("userToken");
    return this.http.post<any>(`${environment.ApiUrl}${path}`, JSON.stringify(payload), { headers: { 'Content-Type': 'application/json; charset=utf-8;', 'token': `${token}` } });
  }
}
