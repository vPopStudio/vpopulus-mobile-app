import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  constructor(public httpClient: HttpClient, private storage: Storage) { }

  login(username: string, password: string): Observable<any> {
    return this.httpClient.get(environment.ApiUrl + "/api/auth/login?email=" + username + "&password=" + password);
  }

  userToken: string;
  canActivate(): boolean {
    this.storage.get("userToken").then(x => {
      this.userToken = x;
    });
    if (this.userToken == undefined)
      return false;
    else
      return true;
  }
}
