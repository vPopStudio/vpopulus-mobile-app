import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class CitizenService {

  userToken: string;

  constructor(private httpClient: HttpClient, private storage: Storage) {
    this.storage.get("userToken").then(x => { this.userToken = x; })
   }

  /* Working */
  getWorkResult() : Observable<any> {
    return this.httpClient.get(environment.ApiUrl + "/api/citizen/GetWorkResult?token=" + this.userToken);
  }

  work() : Observable<any> {
    return this.httpClient.get(environment.ApiUrl + "/api/citizen/work?token=" + this.userToken);
  }

  /* Training */
  getTrainingResult() : Observable<any> {
    return this.httpClient.get(environment.ApiUrl + "/api/citizen/GetTrainingResult?token=" + this.userToken);
  }

  train() : Observable<any> {
    return this.httpClient.get(environment.ApiUrl + "/api/citizen/train?token=" + this.userToken);
  }
}
