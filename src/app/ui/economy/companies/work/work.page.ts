import { Component, OnInit } from '@angular/core';
import { CompanyEmployee } from '../../../../models/economy/companies/company-employee';
import { CitizenWork } from '../../../../models/entities/citizen-work';
import { CitizenService } from '../../../../services/entities/citizen.service';
import { CompanyService } from '../../../../services/economy/company.service';
import { NavController } from '@ionic/angular';
import { GenericHttpService } from '../../../../services/generic-http.service';

@Component({
  selector: 'app-work',
  templateUrl: './work.page.html',
  styleUrls: ['./work.page.scss'],
})
export class WorkPage implements OnInit {

  workResult: any = {};
  companyEmployee: any = {};
  workButtonDisabled: boolean = false;

  constructor(private citizenService: CitizenService, private companyService: CompanyService, private nav: NavController,
    private httpClient: GenericHttpService,
  ) { }

  ngOnInit() {
    this.getEmployment();
    this.getResults();
  }

  work() {
    this.workButtonDisabled = true;
    this.httpClient.post(`/api/citizen/work`, {}).then(r => {
      r.subscribe(x => {
        this.getResults();
        this.workButtonDisabled = false;
      },
        err => { console.log(err); });
    });
  }

  getResults() {
    this.httpClient.get(`/api/citizen/GetWorkResult`).then(r => {
      r.subscribe(x => {
        this.workResult = x;
        console.log(x);
      },
        err => { console.log(err); });
    });
  }

  getEmployment() {
    this.httpClient.get(`/api/company/GetEmployment`).then(r => {
      r.subscribe(x => {
        if (x == null)
          return this.nav.navigateRoot('/home');
        else {
          this.companyEmployee = x;
        }
      },
        err => { console.log(err); });
    });
  }
}
