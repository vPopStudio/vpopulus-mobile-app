import { Component, OnInit } from '@angular/core';
import { Article } from '../../../../models/social/article';
import { NavController } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';
import { ArticleViewPage } from '../article-view/article-view.page';
import { Storage } from '@ionic/storage';
import { ArticleService } from '../../../../services/social/article.service';
import { GenericHttpService } from '../../../../services/generic-http.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.page.html',
  styleUrls: ['./article-list.page.scss'],
})
export class ArticleListPage implements OnInit {

  articles: any[] = [];

  constructor(private nav: NavController, public articleView: ArticleViewPage, private storage: Storage, private articleService:ArticleService, 
    private httpClient: GenericHttpService
    ) { }

  ngOnInit() {
    this.httpClient.get(`/api/article/GetLatest`).then(r => {
      r.subscribe(x => {
        console.log(x);
        this.articles = x;
      }, 
      err => { console.log(err); });
    });
  }

  openArticle(id: number) {
    this.storage.set('article', this.articles.find(x => x.id == id));
    this.nav.navigateRoot('/article-view');
  }

}
