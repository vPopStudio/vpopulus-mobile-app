import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ArticleViewPage } from './article-view.page';
import { SafeContentPipe } from '../../../../utilities/pipes/safe-content.pipe';

const routes: Routes = [
  {
    path: '',
    component: ArticleViewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ArticleViewPage]
})
export class ArticleViewPageModule {}
