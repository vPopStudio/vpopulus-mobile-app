import { Component, OnInit, NgModule } from '@angular/core';
import { Article } from '../../../../models/social/article';
import { Storage } from '@ionic/storage';
import { SafeContentPipe } from '../../../../utilities/pipes/safe-content.pipe';

@Component({
  selector: 'app-article-view',
  templateUrl: './article-view.page.html',
  styleUrls: ['./article-view.page.scss']
})
export class ArticleViewPage implements OnInit {
  article: Article = null;

  constructor(private storage: Storage) { }

  ngOnInit() {
    this.article = new Article(0, 'Loading...', 'Loading...', 'Loading...', 'Loading...', 'Loading...');

    this.storage.get('article').then(x => {
      this.article = x;
      if (this.article == null || this.article == undefined)
        this.article = new Article(0, 'Loading...', 'Loading...', 'Loading...', 'Loading...', 'Loading...');
    });
  }



}
