import { Component, OnInit } from '@angular/core';
import { CitizenTraining } from '../../../../models/entities/citizen-training';
import { CitizenService } from '../../../../services/entities/citizen.service';
import { GenericHttpService } from '../../../../services/generic-http.service';

@Component({
  selector: 'app-train',
  templateUrl: './train.page.html',
  styleUrls: ['./train.page.scss'],
})
export class TrainPage implements OnInit {

  trainButtonDisabled: boolean = false;
  trainingStats: any;

  constructor(
    private httpClient: GenericHttpService,
  ) { }

  ngOnInit() {
    this.getResults()
  }

  train() {
    this.trainButtonDisabled = true;
    this.httpClient.post(`/api/citizen/train`, {}).then(r => {
      r.subscribe(x => {
      this.getResults();
      this.trainButtonDisabled = false;
      }, 
      err => { console.log(err); });
    });
  }

  getResults() {
    this.httpClient.get(`/api/citizen/GetTrainingResult`).then(r => {
      r.subscribe(x => {
        this.trainingStats = x;
      }, 
      err => { console.log(err); });
    });
  }

}
