import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';

import { Storage } from '@ionic/storage';
import { AuthService } from '../../../services/backend/auth.service';
import { AppComponent } from '../../../app.component';
import { GenericHttpService } from '../../../services/generic-http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginButtonDisabled: boolean = false;
  username: string;
  password: string;

  errorMessage: string;

  constructor(
    private nav: NavController,
    private storage: Storage,
    public alertController: AlertController,
    public appComponent: AppComponent,
    private httpService: GenericHttpService,
  ) { }

  ngOnInit() {
  }

  login() {
    if (this.username == undefined || this.password == undefined)
      return;

    this.loginButtonDisabled = true;
    this.httpService.post(`/api/auth/login`, { email: this.username, password: this.password }).then(r => {
      r.subscribe(data => {
        this.loginButtonDisabled = false;
        this.storage.set("userToken", data.guidValue1);
        this.appComponent.userToken = data.guidValue1;
        return this.nav.navigateRoot('/home');
      }, error => {
            console.log(error)
            this.alertController.create({
              message: error.error.message,
              buttons: ['OK']
            }).then(x => x.present());
            this.loginButtonDisabled = false;
      });
    });
  }

}
