export class CitizenTraining {
    strengthIncreased: string;
    daysTrained: number;
    wellnessDuringTraining: number;
}
