export class Article {
    id: number;
    title: string;
    context: string;
    creationDateTime: string;

    newspaperAvatar: string;
    newspaperName: string;

    constructor(id: number, title: string, content: string, date: string, newspaperName: string, newspaperAvatar: string) {
        this.id = id;
        this.title = title;
        this.context = content;
        this.creationDateTime = date
        this.newspaperName = newspaperName;
        this.newspaperAvatar = newspaperAvatar;
    }
}
