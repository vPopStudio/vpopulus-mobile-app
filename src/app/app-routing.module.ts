import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './ui/auth/login/login.module#LoginPageModule' },
  { path: 'train', loadChildren: './ui/entities/citizens/train/train.module#TrainPageModule' },
  { path: 'work', loadChildren: './ui/economy/companies/work/work.module#WorkPageModule' },
  { path: 'articles', loadChildren: './ui/social/articles/article-list/article-list.module#ArticleListPageModule' },
  { path: 'article-view', loadChildren: './ui/social/articles/article-view/article-view.module#ArticleViewPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
